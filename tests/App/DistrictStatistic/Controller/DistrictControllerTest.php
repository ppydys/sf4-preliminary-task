<?php

namespace App\Tests\DistrictStatistic\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class IndexTest
 * @package App\Tests\DistrictStatistic\Controller\DistrictController
 * @coversDefaultClass \App\DistrictStatistic\Controller\DistrictController
 */
class DistrictControllerTest extends WebTestCase
{
    /**
     * @covers ::index()
     */
    public function testIndexSuccess()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/district-statistic');

        $this->assertResponseIsSuccessful();

    }

    /**
     * @covers ::list()
     */
    public function testListSuccess()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/district-statistic/list');

        $this->assertResponseIsSuccessful();

    }

    /**
     * @covers ::refreshData()
     */
    public function testRefreshDataSuccess()
    {
        $client = static::createClient();

        $crawler = $client->request('POST', '/district-statistic/refresh-data');

        $this->assertResponseIsSuccessful();

    }
}