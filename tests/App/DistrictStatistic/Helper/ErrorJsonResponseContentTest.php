<?php

namespace App\DistrictStatistic\Helper;

use PHPUnit\Framework\TestCase;

class ErrorJsonResponseContentTest extends TestCase
{

    public function testGetJsonSuccess()
    {
        $errorJsonResponseContent = new ErrorJsonResponseContent('Test Message');
        $result = $errorJsonResponseContent->getJson();

        $this->assertEquals('Test Message', $result['message']);
        $this->assertEquals('error', $result['status']);
    }
}
