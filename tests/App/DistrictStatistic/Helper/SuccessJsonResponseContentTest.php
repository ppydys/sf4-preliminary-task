<?php

namespace App\DistrictStatistic\Helper;

use PHPUnit\Framework\TestCase;

class SuccessJsonResponseContentTest extends TestCase
{

    public function testGetJson()
    {
        $errorJsonResponseContent = new SuccessJsonResponseContent('Test Message');
        $result = $errorJsonResponseContent->getJson();

        $this->assertEquals('Test Message', $result['message']);
        $this->assertEquals('ok', $result['status']);
    }
}
