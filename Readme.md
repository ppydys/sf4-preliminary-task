
SF4 Project
==============================

The project is **not completed** as desired.

Scope
--------------------------------

What was done:
1. Dev docker environment managed via `make` commands 
2. Initial stricture to get information from external endpoints
3. Initial frontend App 

What was NOT finished:
1. The frontend ( only initial Vue.js app was made)
2. Fully working backend
3. Filtering
4. Sorting
5. Service to refresh data from external endpoints


How to run the project
===================================

Clone the repository, cd into the  project's folder

1. Make an env file 

`ln -s .env.local .env`

2. build a docker environment

`make build`

3.  composer install  

`make backend-composer-install`

4. Run tests

`make backend-phpunit` 

5. Run service in the browser 

`http://localhost:8052/`

**Warning!** Page seen after that shows only the initial check that Vue.js is properly initialized. This page didn't accomplish  project's goals. 
