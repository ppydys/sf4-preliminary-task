PHP_BACKEND_SERVICE := backend-php
NODE_FRONTEND_SERVICE := frontend-node
MYSQL_TEST_SERVICE := db-test
MYSQL_SERVICE := db

build:
	@docker-compose up --build -d --force-recreate

up:
	@docker-compose up -d

down:
	@docker-compose down --volumes

backend-composer-install:
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) composer install
 
backend-composer-update:
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) composer update

backend-composer:
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) composer $(c)

backend-command:
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) ./bin/console $(c)

backend-phpunit:
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) ./bin/phpunit $(c)

backend-bash:
	@docker-compose exec $(PHP_BACKEND_SERVICE) bash $(c)

frontend-bash:
	@docker-compose exec $(NODE_FRONTEND_SERVICE) bash $(c)

frontend-build-dev:
	@docker-compose exec $(NODE_FRONTEND_SERVICE) npm run build-local62

frontend-build-prod:
	@docker-compose exec $(NODE_FRONTEND_SERVICE) npm run build-local62

frontend-watch:
	@docker-compose exec $(NODE_FRONTEND_SERVICE) npm run build-local62

backend-test:
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) vendor/bin/php-cs-fixer fix src --rules=@PSR2 --using-cache=no --dry-run --verbose --diff
	@docker-compose exec -T $(PHP_BACKEND_SERVICE) bin/console security:check

backend-database-reload:
	@docker-compose exec -T $(PHP_KPI_SERVICE) ./bin/console d:d:d --force 
	@docker-compose exec -T $(PHP_KPI_SERVICE) ./bin/console d:d:c 
	@docker-compose exec -T $(PHP_KPI_SERVICE) ./bin/console d:m:m --no-interaction 
	@docker-compose exec -T $(PHP_KPI_SERVICE) ./bin/console d:f:l --append

db-test-bash:
	@docker-compose exec $(MYSQL_TEST_SERVICE) bash $(c)

db-bash:
	@docker-compose exec $(MYSQL_SERVICE) bash $(c)

clean:
	@docker-compose rm --stop --force

ccl:
	chmod 777 -R  ./.data
	chmod 777 -R  ./backend/storage ./backend/bootstrap/cache
	chmod 777 -R  ./devcomm/storage ./devcomm/bootstrap/cache
	chmod 777 -R  ./kpi/var
	rm -rf ./kpi/var/cache/*

all:
	@make -s build
	@make -s composer-install
	@make -s database
	@make -s test
	@make -s down
	@make -s clean
