<?php

namespace App\DistrictStatistic\Controller;

use App\DistrictStatistic\Helper\SuccessJsonResponse;
use App\DistrictStatistic\Helper\SuccessJsonResponseContent;
use App\DistrictStatistic\Service\RefreshDistrictDataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class DistrictController
 * @package App\DistrictStatistic\Controller
 * @Route( "/district-statistic")
 */
class DistrictController extends AbstractController
{
    private SerializerInterface $serializer;
    private RefreshDistrictDataService $refreshDistrictDataService;

    public function __construct(
        SerializerInterface $serializer,
        RefreshDistrictDataService $refreshDistrictDataService
    ) {
        $this->serializer = $serializer;
        $this->refreshDistrictDataService = $refreshDistrictDataService;
    }


    /**
     * @Route("", name="district-index", methods={"get"})
     */
    public function index(): Response
    {
        return $this->render('DistrictStatistic/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/list", name="district-list", methods={"get"})
     */
    public function list(): Response
    {
        $districtStatistics = [];

        return $this->json($this->serializer->serialize(
            $districtStatistics,
            'json'
        ));
    }

    /**
     * @Route("/refresh-data", name="district-refresh-data", methods={"post"})
     */
    public function refreshData(): Response
    {

        $this->refreshDistrictDataService->refreshData();


        $response = new SuccessJsonResponseContent();

        return $this->json($this->serializer->serialize(
            $response->getJson(),
            'json'
        ));
    }
}
