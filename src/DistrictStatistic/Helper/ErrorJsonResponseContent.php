<?php

namespace App\DistrictStatistic\Helper;

class ErrorJsonResponseContent
{
    const STATUS = 'error';
    /**
     * @var string
     */
    private string $message;

    public function __construct($message = 'Success')
    {
        $this->message = $message;
    }

    public function getJson()
    {
        return [
            'message' => $this->message,
            'status' => self::STATUS
        ];
    }
}