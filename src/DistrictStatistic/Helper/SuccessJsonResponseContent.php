<?php

namespace App\DistrictStatistic\Helper;

class SuccessJsonResponseContent
{
    const STATUS = 'ok';
    /**
     * @var string
     */
    private string $message;

    public function __construct($message = 'Success')
    {
        $this->message = $message;
    }

    public function getJson()
    {
        return [
            'message' => $this->message,
            'status' => self::STATUS
        ];
    }
}