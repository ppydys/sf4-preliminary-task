<?php

namespace App\DistrictStatistic\Service;

class RefreshDistrictDataService
{
    /**
     * @var DistrictDataServiceInterface[]
     */
    private iterable $handlers;

    public function __construct(iterable $handlers)
    {
        $tmp = $handlers;
        $this->handlers = $handlers instanceof \Traversable ? iterator_to_array($handlers) : $handlers;
    }

    public function refreshData()
    {
        foreach ($this->handlers as $handler) {
            $handler->getData();
        }

    }
}