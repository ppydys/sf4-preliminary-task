<?php


namespace App\DistrictStatistic\Service;


interface DistrictDataServiceInterface
{
    public function getData();
}