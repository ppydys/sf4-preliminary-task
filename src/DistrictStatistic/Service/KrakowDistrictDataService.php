<?php

namespace App\DistrictStatistic\Service;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class KrakowDistrictDataService implements DistrictDataServiceInterface
{

    const URL = 'https://appimeri.um.krakow.pl/app-pub-dzl/pages/DzlViewAll.jsf?a=1&set=1&lay=theme-normal&font=font-size-normal';
    const URL_OPTION_PATTERN = 'https://appimeri.um.krakow.pl/app-pub-dzl/pages/DzlViewGlw.jsf?id=%d&lay=theme-normal&fo=&submit=Wybierz';
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getData()
    {
        $response = $this->client->request(
            'GET',
            self::URL,
            $this->getOptions()
        );
        $statusCode = $response->getStatusCode();
        $content = $response->getContent();


        $crawler = new Crawler($content);

        $crawler = $crawler->filter('div#mainDiv > form > select');

        $availableOptions = [];
        foreach ($crawler as $domElement) {
            foreach ($domElement->childNodes as $option) {
                $optionValue = $option->getAttribute('value');
                $optionText = $option->textContent;
                $availableOptions[] = [
                    'value' => $optionValue,
                    'text' => $optionText,
                ];
            }
        }


        //iterate over all districts
        foreach ($availableOptions as $districtOption) {
            $districtHtmlData = $this->getAskForDistrict($districtOption);

            $crawler = new Crawler($content);

            $crawler = $crawler->filter('table > tr > td > table > tr > td');

            foreach ($crawler as $tdElement) {
                $content = $tdElement->textContent;

            }
        }

        return $content;
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Exception
     */
    private function getAskForDistrict($districtOption)
    {
        return $this->getHtmlContent(sprintf(self::URL_OPTION_PATTERN, $districtOption['value']));
    }

    private function getOptions(): array
    {
        return [
            'verify_peer' => false,  // see https://php.net/context.ssl for the following options
            'verify_host' => false,
        ];
    }


    private function getHtmlContent($url): string
    {
        $response = $this->client->request(
            'GET',
            $url,
            $this->getOptions()
        );
        $statusCode = $response->getStatusCode();
        if ($statusCode !== Response::HTTP_OK) {
            throw new \Exception("Source server response error");
        }

        return $response->getContent();
    }
}